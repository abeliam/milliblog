<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CreatePostRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function index() {
        $posts = Post::with('author:id,username,name')->orderBy('created_at', 'DESC')->paginate(5);
        return response()->json($posts);
    }

    public function create(Request $request) {
        Post::create([
            'author_id' => $request->user()->id,
            'body' => $request->input('body')
        ]);
        return response('OK', 200)
                          ->header('Content-Type', 'text/plain');;
    }
}
