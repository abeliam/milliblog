import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import PostComponent from './components/PostComponent.vue'
import PostListComponent from './components/PostListComponent.vue'
import PostCreateComponent from './components/PostCreateComponent.vue'

async function get(url) {
    const response = await fetch(url, {
        credentials: 'same-origin',
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        }
    })
    if (response.ok) {
        return await response.json()
    }

    const error = await response.json()

    throw error.message
}

async function post(url, body) {
    const response = await fetch(url, {
        credentials: 'same-origin',
        method: "POST",
        body: JSON.stringify(body),
        headers: {
            'Content-Type': 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        }
    })
    console.log(body, response)
    if (response.ok) {
        return await response.json()
    }

    const error = await response.json()

    throw error.message
}

const api = {
    get,
    post
}

Vue.use(Vuex)
Vue.use(VueRouter)

const store = new Vuex.Store({

    state: {
        user: {
            logged: false,
            data: {}
        },
        posts: {
            current_page: 1,
            last_page: 0,
            posts: []
        }
    },
    getters: {
        posts(state) {
            return state.posts.posts
        },
        logged(state) {
            return state.user.logged
        }
    },
    mutations: {
        addPosts(state, posts) {
            state.posts.posts = state.posts.posts.concat(posts)
        },
        currentPagePosts(state, page) {
            state.posts.current_page = page
        },
        lastPagePosts(state, page) {
            state.posts.last_page = page
        },
        setUserData(state, data) {
            state.user.data = data
            state.user.logged = true
        }
    },
    actions: {
        createPost: async function (context, body) {
            await api.post('/api/posts', {
                author_id: context.state.user.data.id,
                body
            })
        },
        getPosts: async function (context, page) {
            const response = await api.get(`/api/posts?page=${page}`)
            context.commit('addPosts', response.data)
            context.commit('currentPagePosts', response.current_page)
            context.commit('lastPagePosts', response.last_page)
        },
        getUser: async function (context) {
            try {
                const response = await api.get('/api/user')
                context.commit('setUserData', response)
            } catch(e) {}
        }
    }
})

const routes = [

]

const router = new VueRouter({
    mode: 'history',
    routes
})

Vue.component('post', PostComponent)
Vue.component('post-list', PostListComponent)
Vue.component('post-create', PostCreateComponent)

const app = new Vue({
    components: {
        PostComponent,
        PostListComponent,
        PostCreateComponent
    },
    store,
    router
}).$mount('#app')
