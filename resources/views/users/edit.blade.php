@extends('layout')

@section('content')
    <section class="container">
        <h1 class="title">My profile</a></h1>
        <form action="{{ route('users.update', ['id' => $user->id]) }}" method="post">
            @csrf
            @method('patch')
            <div class="field">
                <label for="username">Username</label>
                <input type="text" id="username" name="username" value="{{ old('username', $user->username) }}">
                @if ($errors->has('username'))
                    <div class="error" role="alert">
                        {{ $errors->first('username') }}
                    </div>
                @endif
            </div>
            <div class="field">
                <label for="name">Display username</label>
                <input type="text" id="name" name="name" value="{{ old('name', $user->name) }}">
                @if ($errors->has('name'))
                    <div class="error" role="alert">
                        {{ $errors->first('name') }}
                    </div>
                @endif
            </div>
            <div class="field">
                <label for="email">Email address</label>
                <input type="email" id="email" name="email" value="{{ old('email', $user->email) }}">
                @if ($errors->has('email'))
                    <div class="error" role="alert">
                        {{ $errors->first('email') }}
                    </div>
                @endif
            </div>
            <button class="button" type="submit">Save</button>
        </form>
    </section>

@endsection
