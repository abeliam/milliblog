@extends('layout')

@section('content')
    <section class="container">
        <h1 class="title">Users</h1>
        <div class="users">
            @foreach($users as $user)
                <a href="{{ route('users.show', ['id' => $user->id]) }}" class="user">
                    <span>{{ $user->name }}</span>
                    <span class="at">{{ '@' . $user->username }}</span>
                </a>
            @endforeach
        </div>
        {{ $users->links('partials.pagination') }}
    </section>

@endsection
