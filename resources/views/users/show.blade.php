@extends('layout')

@section('content')
    <section class="container">
        <h1 class="title">{{ $user->name }} <a href="{{ route('users.show', $user->id) }}" class="at">{{ '@' . $user->username }}</a></h1>
        @if(Auth::id() == $user->id)
            <div class="button-group">
                <a href="{{ route('users.edit', ['id' => $user->id]) }}" class="green button">Edit</a>
            </div>
        @endif
        @include('partials.posts', ['posts' => $user->posts])
    </section>

@endsection
