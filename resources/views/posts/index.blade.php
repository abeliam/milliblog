@extends('layout')

@section('content')
    <router-view></router-view>
    <post-list></post-list>
@endsection
