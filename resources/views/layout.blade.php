<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>milliblog</title>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Caption" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/master.css">
</head>
<body>
    <div id="app">
        <div id="mainHeaderWrapper">
            <header id="mainHeader" class="container">
                <h1><a href="{{ route('home') }}"><i class="fas fa-cookie"></i> milliblog</a></h1>
                <div class="fg1"></div>
                @if(Auth::check())
                    <a href="{{ route('logout') }}" class="red button" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fas fa-sign-out-alt"></i> Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @else
                    <a href="{{ route('register') }}" class="green button">Sign Up</a>
                    <a href="{{ route('login') }}" class="button"><i class="fas fa-sign-in-alt"></i> Sign In</a>
                @endif
            </header>
        </div>
        <div id="main">
            <div class="fg1"></div>
            <nav class="panel">
            </nav>
            @yield('content')
            <nav class="panel">
                <a href="{{ route('users.index') }}">Users</a>
            </nav>
            <div class="fg1"></div>
        </div>
        <footer id="mainFooter" class="container">
            <p>powered by milliblog microblogging engine</p>
        </footer>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
